const express = require('express')
const router = express.Router()
const Album = require('../models/Album')

// Retorna todos os albuns
router.get('/', async (req, res, next) => {
    try{
        let filter = {}
        if(req.query.ano) filter.ano = req.query.ano

        const limit = Math.min(parseInt(req.query.limit), 10) || 10
        const skip = parseInt(req.query.skip) || 0
        let albuns = []
        albuns = await Album.find(filter).limit(limit).skip(skip)
        res.json(albuns)
    }catch (err){
        next(err)
    }
})

// Retorna apenas um album baseado no ID
router.get('/:id', async (req, res, next) => {
    try{
        const id = req.params.id
        let album = await Album.findById(id).populate('artista')
        if(!album){
            res.statusCode = 404
            throw new Error("O objeto procurado não foi encontrado")
        }
        res.json(album)
    } catch(err){
        next(err)
    }
})

// Salva um objeto enviado através do body
router.post('/', async (req, res, next) => {
    try{
        const album = new Album(req.body)
        const resultado = await album.save()
        res.json(resultado)
    } catch(err){
        next(err)
    }
})

// Modifica uma parte do album de acordo com o ID e o body
router.put('/:id', async (req, res, next) => {
    try{
        const id = req.params.id
        const alBody = req.body
        const resultado = await Album.findByIdAndUpdate(id, alBody)
        res.json(resultado)
    } catch (err){
        next(err)
    }
})

router.delete('/:id', async (req, res, next) => {
    try{
        const id = req.params.id
        const resultado = await Album.findByIdAndDelete(id)
        res.json(resultado)
    } catch (err){
        next(err)
    }
})

module.exports = router