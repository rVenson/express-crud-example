const express = require('express')
const router = express.Router()
const Artista = require('../models/Artista')

router.get('/', async (req, res, next) => {
    try{
        const doc = await Artista.find()
        res.json(doc)
    } catch (err){
        next(err)
    }
})

router.get('/:id', async (req, res, next) => {
    try{
        const id = req.params.id
        const doc = await Artista.findById(id)
        res.json(doc)
    } catch (err){
        next(err)
    }
})

router.post('/', async (req, res, next) => {
    try{
        const body = req.body
        const doc = new Artista(body)
        await doc.save()
        res.json(doc)
    } catch (err){
        next(err)
    }
})

router.put('/:id', async (req, res, next) => {
    try{
        const body = req.body
        const id = req.params.id
        const doc = await Artista.findByIdAndUpdate(id, body)
        res.json(doc)
    } catch (err){
        next(err)
    }
})

router.delete('/:id', async (req, res, next) => {
    try{
        const id = req.params.id
        const doc = await Artista.findByIdAndDelete(id)
        res.json(doc)
    } catch (err){
        next(err)
    }
})

module.exports = router